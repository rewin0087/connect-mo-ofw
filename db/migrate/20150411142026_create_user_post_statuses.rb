class CreateUserPostStatuses < ActiveRecord::Migration
  def change
    create_table :user_post_statuses do |t|
      t.string :status
      t.string :remarks
      t.references :user, index: true
      t.references :post, index: true

      t.timestamps
    end
  end
end
