class CreateCommunities < ActiveRecord::Migration
  def change
    create_table :communities do |t|
      t.string :name
      t.string :description
      t.string :location
      t.string :latitude
      t.string :longitude
      
      t.timestamps
    end
  end
end
