class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.text :description
      t.string :latitude
      t.string :longitude
      t.string :requirements
      t.string :address
      t.string :price
      t.string :category
      t.references :user, index: true

      t.timestamps
    end
  end
end
