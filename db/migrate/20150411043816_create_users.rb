class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :full_name
      t.string :email
      t.string :gender
      t.string :hometown
      t.string :current_location
      t.string :current_latitude
      t.string :current_longitude
      t.string :last_longitude
      t.string :last_latitude
      t.string :fbid
      t.string :account_type
      t.string :small_picture
      t.string :big_picture
      t.string :birthday
      t.string :website
      t.string :address
      t.string :contact
      
      t.timestamps
    end
  end
end
