class CreateUserCommunities < ActiveRecord::Migration
  def change
    create_table :user_communities do |t|
      t.references :user, index: true
      t.references :community, index: true

      t.timestamps
    end
  end
end
