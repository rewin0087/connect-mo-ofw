class CreateRatings < ActiveRecord::Migration
  def change
    create_table :ratings do |t|
      t.integer :points
      t.references :user, index: true
      t.references :post, index: true

      t.timestamps
    end
  end
end
