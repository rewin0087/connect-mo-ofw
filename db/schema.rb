# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150411142026) do

  create_table "comments", force: true do |t|
    t.string   "message"
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["post_id"], name: "index_comments_on_post_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "communities", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "location"
    t.string   "latitude"
    t.string   "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "posts", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "requirements"
    t.string   "address"
    t.string   "price"
    t.string   "category"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "ratings", force: true do |t|
    t.integer  "points"
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ratings", ["post_id"], name: "index_ratings_on_post_id", using: :btree
  add_index "ratings", ["user_id"], name: "index_ratings_on_user_id", using: :btree

  create_table "user_communities", force: true do |t|
    t.integer  "user_id"
    t.integer  "community_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_communities", ["community_id"], name: "index_user_communities_on_community_id", using: :btree
  add_index "user_communities", ["user_id"], name: "index_user_communities_on_user_id", using: :btree

  create_table "user_post_statuses", force: true do |t|
    t.string   "status"
    t.string   "remarks"
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_post_statuses", ["post_id"], name: "index_user_post_statuses_on_post_id", using: :btree
  add_index "user_post_statuses", ["user_id"], name: "index_user_post_statuses_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "full_name"
    t.string   "email"
    t.string   "gender"
    t.string   "hometown"
    t.string   "current_location"
    t.string   "current_latitude"
    t.string   "current_longitude"
    t.string   "last_longitude"
    t.string   "last_latitude"
    t.string   "fbid"
    t.string   "account_type"
    t.string   "small_picture"
    t.string   "big_picture"
    t.string   "birthday"
    t.string   "website"
    t.string   "address"
    t.string   "contact"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
