module Here
  class Reverse_Geocoder
    include Her::Model
    use_api REVERSE_GEOCODER_API

    def self.reverse(params)
      query = {
        app_id: ENV['HERE_MAP_APP_ID'],
        app_code: ENV['HERE_MAP_APP_CODE'],
        gen: 8,
        pos: "#{params[:lat]},#{params[:lng]}",
        mode: 'trackPosition'
      }.to_param

      reverse = self.get("reversegeocode.json?#{query}")
      reverse.Response[:View][0][:Result][0][:Location][:Address][:Label]
    end
  end
end