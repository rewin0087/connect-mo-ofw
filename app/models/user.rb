class User < ActiveRecord::Base
  has_many :user_communities
  has_many :user_post_statuses
  has_many :posts
  has_many :communities, through: :user_communities

  def ofw_user
    'Ofw'
  end

  def agency_user
    'Agency'
  end

  def application_status(post_id)
    user_post_statuses.where({ post_id: post_id })
  end

  def self.find_agency(id)
    find(id)
  end
end
