class UserPostStatus < ActiveRecord::Base
  belongs_to :user
  belongs_to :post

  ALL = %w(applied pending accepted missing_files interview)

  ALL.each do |state|
    define_method "#{state}?" do
      status == state.to_s
    end

    define_method "#{state}!" do
      status = state
    end
  end

end
