module Chikka
  class Send_Api
    include Her::Model
    use_api SEND_CHIKKA_API
    collection_path "smsapi/request"

    def self.deliver(params)
			x = Digest::MD5.new
	  	num = x.update Time.now.strftime('%Y%m%d%H%M%S%L')
			mess_id = num.hexdigest

      query = {
        client_id: ENV['CHIKKA_CLIENT_ID'],
        secret_key: ENV['CHIKKA_SECRET_KEY'],
        message_type: 'SEND',
        mobile_number: params[:number],
        shortcode: ENV['CHIKKA_SHORTCODE'],
        message_id: mess_id,
        message: params[:message]
      }

      self.post("#{collection_path}", query)
    end

    def self.received_text(params)
      x = Digest::MD5.new
      num = x.update Time.now.strftime('%Y%m%d%H%M%S%L')
      res_id = num.hexdigest

      query = {
        client_id: ENV['CHIKKA_CLIENT_ID'],
        secret_key: ENV['CHIKKA_SECRET_KEY'],
        message_type: 'incoming',
        mobile_number: params[:number],
        shortcode: ENV['CHIKKA_SHORTCODE'],
        request_id: res_id,
        message: params[:message],
        timestamp: num
      }
    end
  end
end