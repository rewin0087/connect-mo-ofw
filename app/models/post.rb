class Post < ActiveRecord::Base
  belongs_to :user
  has_many :comments
  has_many :ratings
  has_many :user_post_statuses

  def self.recent
    order('created_at DESC').limit(10)
  end

  def self.applicants(agency_id)
    posts = where({user_id: agency_id})
    applicants = []
   
    posts.map do |post|
        user_status = post.user_post_statuses.group('post_id').group('user_id')
        user_status.each do |status|
          applicants.push(status.user.user_post_statuses.last)
        end
    end 

    applicants
  end

  def related_post
    user.posts.map { |p| p if p.category == category }.compact
  end

  def job?
    category == 'job'
  end

  def rent?
    category == 'rent'
  end
end
