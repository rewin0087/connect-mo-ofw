module HomeHelper
  def active?(params, mode)
    'active' if params[:active_tab] == mode
  end
end
