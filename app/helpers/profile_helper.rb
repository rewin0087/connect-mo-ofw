module ProfileHelper
  def colorize(status)
    if status == 'Viewed'
      'label-info'
    elsif status == 'Accepted'
      'label-warning'
    elsif status == 'Missing Files'
      'label-danger'
    elsif status == 'Interview'
      'label-success'
    else
      'label-primary'
    end
  end
end
