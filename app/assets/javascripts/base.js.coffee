window.getCurrentUserLocation = () ->
  navigator.geolocation.getCurrentPosition(setPosition) if navigator.geolocation

window.setPosition = (position) ->
  latitude = position.coords.latitude
  longitude = position.coords.longitude

  window.current_location = { latitude: latitude, longitude: longitude }

  if !$('body').hasClass('authentication')
    loader(true) if $('body').hasClass('home') && !gon.geoSession.lat && !gon.geoSession.lng
    
    $.post '/api/users', { latitude: current_location.latitude, longitude: current_location.longitude }, (response) ->
      if $('body').hasClass('home') && !gon.geoSession.lat && !gon.geoSession.lng
        Turbolinks.visit(window.location.href)
        loader(false)

window.loader = (show) ->
  loader = $('.loader')

  if(show)
    loader.show()
  else
    loader.hide()

window.platform = new H.service.Platform app_id: gon.here.app_id, app_code: gon.here.app_code, useCIT: true unless $('body').hasClass('authentication')

$(document).on 'page:load ready', () ->
  unless $('body').hasClass('authentication')
    getCurrentUserLocation()

    search_field = $('input#place_keyword')
    search_button = $('button#search_button')

    search_field.on 'keyup', (e) ->
      keyword = e.currentTarget.value
      if(e.keycode == 13 || e.which == 13)
        url = 'locations?q=' + keyword
        Turbolinks.visit(url)

    search_button.on 'click', (e) ->
      keyword = search_field.val()
      url = 'locations?q=' + keyword
      Turbolinks.visit(url)

$(document).on 'page:fetch', (e) ->
  loader(true)

$(document).on 'page:receive page:restore', (e) ->
  loader(false)
