# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

addInfoBubble = (items, hereMap) ->
  group = new H.map.Group()
  map.addObject(group)

  group.addEventListener 'tap', (evt) =>
    hereMap.openBubble(evt.target.getPosition(), evt.target.getData())
  , false

  items.map (e) =>
    if !!e
      addMarkerToGroup(group, { lat: e.latitude, lng: e.longitude },
		    "<div class='title'><a href=/home/" + e.id + "?active_tab=public_transport_route>" + e.title +
		    "</a></div><div class='vicinity' >" + e.description +
		    "</div>")

addMarkerToGroup = (group, coordinate, html) ->
  marker = new H.map.Marker(coordinate)
  marker.setData(html)
  group.addObject(marker)


$(document).on 'page:load page:ready ready', () ->
  if $('body').hasClass('home') && $('div').hasClass('map')
    defaultLayers = platform.createDefaultLayers()

    window.map = new H.Map(document.getElementById('map'),
    	defaultLayers.normal.map, {
    		center:
    			lat: gon.geoSession.lat,
    			lng: gon.geoSession.lng
    		,
    		zoom: 13
    	}
    )

    window.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map))

    window.ui = H.ui.UI.createDefault(map, defaultLayers)

    hereMap = new HereMap()
    hereMap.map = map
    hereMap.ui = ui
    hereMap.plotCurrentLocation(gon.geoSession.lat + "," + gon.geoSession.lng)

    addInfoBubble(gon.recentPosts, hereMap)

  # map
  if $('div').hasClass('post-maps')
    mapWithPublicTransportRoute(platform, 'fastest;publicTransport')
    mapWithDrivingroute(platform, 'fastest;car')
    mapWithPedestrianRoute(platform, 'shortest;pedestrian')

    $('#map-tabs a').on 'click', (e) =>
      e.preventDefault()
      dis = $(e.currentTarget)
      
      url = window.location.href.split('?')

      query = url[1].split('&')
      domain = "#{url[0]}?"
      active_tabs = ['active_tab=public_transport_route', 'active_tab=driving_route', 'active_tab=pedestrian_route']
      new_query = []
      $.each query, (i, param) ->
        new_query.push(param) unless active_tabs.indexOf(param) != -1
      new_url = "#{domain}#{new_query.join('&')}"
      Turbolinks.visit(new_url + '&active_tab=' + dis.attr('data-tab'))

  if $('section').hasClass('new-post-map')
    defaultLayers = platform.createDefaultLayers()

    window.map = new H.Map(document.getElementById('map'),
      defaultLayers.normal.map, {
        center:
          lat: gon.geoSession.lat,
          lng: gon.geoSession.lng
        ,
        zoom: 13
      }
    )

    window.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map))

    window.ui = H.ui.UI.createDefault(map, defaultLayers)

    hereMap = new HereMap()
    hereMap.map = map
    hereMap.ui = ui
    hereMap.plotCurrentLocation(gon.geoSession.lat + "," + gon.geoSession.lng)

  $('#post').on 'click', (e) ->
    alert('Thank you for creating this post.')

  $('#post-and-blast').on 'click', (e) ->
    loader(true)
    $.get '/home/new', {new_post: true}, (success) ->
        loader(false)
        alert('Message has been sent.')
        Turbolinks.visit('/home')