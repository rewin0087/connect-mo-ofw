
window.ws = new WebSocketRails(window.document.location.host + '/websocket')

$(document).on 'page:load page:ready ready', () ->
  
  chatBox = $('div.direct-chat')
  chat = ws.subscribe('community_chat')
  chat.bind 'receive', (data) ->
    if data.user_id != $('button#chat_button').attr('data-user')
      template = '<div class="direct-chat-msg right"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-right">' + data.name + '</span></div><img src="' + data.image + '" class="direct-chat-img"><div class="direct-chat-text">' + data.message+ '</div></div>'
      chatBox.find('div.direct-chat-messages').append(template)

  $('body').on 'click', 'button#chat_button', (e) ->
    me = $(@)
    name = me.attr('data-name')
    message = $('input#chat_message').val()
    image = gon.userPicture
    user_id = me.attr('data-user')
    data = { image: image, name: name, message: message, user_id: user_id }

    template = '<div class="direct-chat-msg"><div class="direct-chat-info clearfix"><span class="direct-chat-name pull-left">' + name + '</span></div><img src="' + image + '" class="direct-chat-img"><div class="direct-chat-text">' + message + '</div></div>'
    
    if message != ''
      $.post '/communities', data, (success) ->
        chatBox.find('div.direct-chat-messages').append(template)
        $('input#chat_message').val('')
