$(document).on 'page:load ready', () ->
  $('body').on 'click', 'a.change_status', (e) ->
    me = $(@)
    status = me.attr('data-status')
    user = me.attr('data-user')
    post = me.attr('data-post')

    $.post '/profile', { status: status, user: user, post: post, change_status: 1 }, (success) ->
      Turbolinks.visit(location.href)