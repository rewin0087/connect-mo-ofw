class window.HereMap
  map: null
  bubble: null
  ui: null
  group: null

  constructor: () ->
    @group = new H.map.Group()

  onSuccess: (result) =>
    if !!result
      route = result.response.route[0]
      @addRouteShapeToMap(route)
      @addManueversToMap(route)

  onError: (error) =>
    console.log("Error" + error)

  openBubble: (position, text) =>
    @bubble = new H.ui.InfoBubble(position, { content: text })
    @bubble.setPosition(position)
    @ui.addBubble(@bubble)
    @bubble.open()

  addRouteShapeToMap: (route) =>
    strip = new H.geo.Strip()
    routeShape = route.shape

    routeShape.forEach (point) ->
      parts = point.split(',')
      strip.pushLatLngAlt(parts[0], parts[1])

    polyline = new H.map.Polyline(strip, {
      style: {
        lineWidth: 4,
        strokeColor: 'rgba(221, 75, 57, 0.8)'
      }
    })

    @map.addObject(polyline)
    @map.setViewBounds(polyline.getBounds(), true)

  addManueversToMap: (route) =>
    svgMarkup = '<svg width="18" height="18" xmlns="http://www.w3.org/2000/svg"><circle cx="8" cy="8" r="8" fill="#dd4b39" stroke="white" stroke-width="1"  /></svg>'
    dotIcon = new H.map.Icon(svgMarkup, { anchor: { x: 8, y: 8 } })
    
    $.each route.leg, (i,v) =>
      $.each v.maneuver, (i, j) =>
        
        marker = new H.map.Marker(
          {
            lat: j.position.latitude,
            lng: j.position.longitude
          },
          {
            icon: dotIcon
          }
        )

        marker.instruction = j.instruction
        @group.addObject(marker)

    @group.addEventListener 'tap', (evt) =>
      @map.setCenter(evt.target.getPosition())
      @openBubble(evt.target.getPosition(), evt.target.instruction)
    , false

    @map.addObject(@group)

  plotToMap: (coords, icon) =>
    coords = coords.split(',')
    domIcon = new H.map.DomIcon icon,
      onAttach: (clonedElement, domIcon, domMarker) ->
        clonedElement.addEventListener 'mouseover', (evt) ->
          evt.target.style.opacity = 0.8
        clonedElement.addEventListener 'mouseout', (evt) ->
          evt.target.style.opacity = 1
      onDetach: (clonedElement, domIcon, domMarker) ->
        clonedElement.removeEventListener 'mouseover', (evt) ->
          evt.target.style.opacity = 0.8
        clonedElement.removeEventListener 'mouseout', (evt) ->
          evt.target.style.opacity = 1

    marker = new H.map.DomMarker({lat: coords[0], lng: coords[1]}, { icon: domIcon })
    @map.addObject(marker)
    marker

  plotCurrentLocation: (coords) =>
    icon = "<div class='marker'><img src='" + gon.userPicture + "' id='user-image'/></div>"
    @plotToMap(coords, icon)

  plotDistination: (coords) =>
    coords = coords.split(',')
    marker = new H.map.Marker({lat: coords[0], lng: coords[1]})
    @map.addObject(marker)
    marker