window.mapWithPublicTransportRoute = (platform, mode) ->
  # load routes
  defaultLayers = platform.createDefaultLayers()
  current_coords = "#{gon.geoSession.lat},#{gon.geoSession.lng}"
  coords = "#{gon.postLocation.latitude},#{gon.postLocation.longitude}"

  map = new H.Map(document.getElementById('map_with_public_transport_route'),
    defaultLayers.normal.map,
    center: 
      lat: gon.geoSession.lat
      lng: gon.geoSession.lng
    zoom: 13
  )

  behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map))
  ui = H.ui.UI.createDefault(map, defaultLayers)

  router = platform.getRoutingService()
  routeRequestParams = {
    mode: mode,
    representation: 'display',
    waypoint0: current_coords,
    waypoint1: coords
  }

  hereMap = new HereMap()
  hereMap.map = map
  hereMap.ui = ui
  hereMap.plotCurrentLocation(current_coords)
  hereMap.plotDistination(coords)

  router.calculateRoute(routeRequestParams, hereMap.onSuccess, hereMap.onError)

window.mapWithDrivingroute = (platform, mode) ->
  # load routes
  defaultLayers = platform.createDefaultLayers()
  current_coords = "#{gon.geoSession.lat},#{gon.geoSession.lng}"
  coords = "#{gon.postLocation.latitude},#{gon.postLocation.longitude}"

  map = new H.Map(document.getElementById('map_with_driving_route'),
    defaultLayers.normal.map,
    center: 
      lat: gon.geoSession.lat 
      lng: gon.geoSession.lng
    zoom: 13
  )

  behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map))
  ui = H.ui.UI.createDefault(map, defaultLayers)

  router = platform.getRoutingService()
  routeRequestParams = {
    mode: mode,
    representation: 'display',
    waypoint0: current_coords,
    waypoint1: coords
  }

  hereMap = new HereMap()
  hereMap.map = map
  hereMap.ui = ui
  hereMap.plotCurrentLocation(current_coords)
  hereMap.plotDistination(coords)

  router.calculateRoute(routeRequestParams, hereMap.onSuccess, hereMap.onError)

window.mapWithPedestrianRoute = (platform, mode) ->
  # load routes
  defaultLayers = platform.createDefaultLayers()
  current_coords = "#{gon.geoSession.lat},#{gon.geoSession.lng}"
  coords = "#{gon.postLocation.latitude},#{gon.postLocation.longitude}"
  
  map = new H.Map(document.getElementById('map_with_pedestrian_route'),
    defaultLayers.normal.map,
    center: 
      lat: gon.geoSession.lat 
      lng: gon.geoSession.lng
    zoom: 13
  )

  behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map))
  ui = H.ui.UI.createDefault(map, defaultLayers)

  router = platform.getRoutingService()
  routeRequestParams = {
    mode: mode,
    representation: 'display',
    waypoint0: current_coords,
    waypoint1: coords
  }

  hereMap = new HereMap()
  hereMap.map = map
  hereMap.ui = ui
  hereMap.plotCurrentLocation(current_coords)
  hereMap.plotDistination(coords)
  
  router.calculateRoute(routeRequestParams, hereMap.onSuccess, hereMap.onError)