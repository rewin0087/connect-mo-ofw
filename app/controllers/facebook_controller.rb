class FacebookController < ApplicationController
  def sign_in
    if code = params[:code]
      access_token = @oauth.get_access_token(code)
      store_facebook_access_token(access_token)
      retreive_current_user_data!
      return redirect_to root_path, turbolinks: true
    end

    redirect_to root_path, turbolinks: true if facebook_access_token
  end

  def sign_out
    reset_session
    redirect_to root_path, turbolinks: true
  end

  private
    def store_facebook_access_token(access_token)
      reset_session
      session[:facebook_access_token] = access_token
      session[:sign_in_time] = Time.zone.now
    end

    def retreive_current_user_data!
      current_user = facebook.get_object('me', {}, api_version: ENV['FACEBOOK_API_VERSION'])

      if user_data = User.find_by_fbid(current_user['id'])
        session[:current_user] = user_data.as_json
      else
        register_user(current_user)
      end
    end

    def register_user(current_user)
      user = User.new
      user.fbid = current_user['id']
      user.first_name = current_user['first_name']
      user.last_name = current_user['last_name']
      user.email = current_user['email']
      user.full_name = current_user['name']
      user.account_type = user.ofw_user
      user.gender = current_user['gender']
      user.hometown = current_user['hometown']['name']
      user.current_location = current_user['location']['name']
      user.small_picture = facebook.get_picture(current_user['id'], { type: 'small'})
      user.big_picture = facebook.get_picture(current_user['id'], { type: 'normal'})
      user.birthday = current_user['birthday']
      user.save

      session[:current_user] = user.as_json
    end
end