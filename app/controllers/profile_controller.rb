class ProfileController < ApplicationController
  def show
  end
  
  def applicants
    @applicants = Post.applicants(@current_user.id)
  end

  def posts

  end

  def buy_credits
  end

  def inquiry
  end

  def create
    if params[:change_status] == '1'
      UserPostStatus.create({ status: params[:status], user_id: params[:user], post_id: params[:post] })
      flash.now[:success] = "Status has been updated to #{params[:status]}"
      post = Post.find params[:post]
      user = User.find params[:user]
      ::Chikka::Send_Api.deliver({message: "Status has been updated for this #{post.category} - #{post.title} into #{params[:status]}", number: user.contact})
      ::Chikka::Send_Api.deliver({message: "Post Owner Notification: Status has been updated for this #{post.category} - #{post.title} into #{params[:status]}", number: @current_user.contact})
      return render text: nil
    end
  end
end
