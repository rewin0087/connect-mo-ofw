class CommunitiesController < ApplicationController
  def index
  end

  def show
    @community = Community.find params[:id]
  end

  def create
    WebsocketRails[:community_chat].trigger 'receive', params
    render text: nil
  end
end
