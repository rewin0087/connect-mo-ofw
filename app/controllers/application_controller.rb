class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  rescue_from StandardError, with: :show_error
  rescue_from Faraday::ConnectionFailed, with: :api_inaccessible 
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  
  before_action :load_current_user_data!
  before_action :send_variables_to_js
  before_filter :initialize_facebook!
  helper_method :sign_in_time
  
  protected
    def send_variables_to_js
      gon.clear
      gon.here = { app_id: ENV['HERE_MAP_APP_ID'], app_code: ENV['HERE_MAP_APP_CODE'] }
      gon.visitor_location = visitor_location
      gon.geo_session = { lat: session[:current_latitude], lng: session[:current_longitude] }
      gon.user_picture = @profile_small_picture
    end

    def record_not_found(exception)
      flash.now[:error] = 'No Record Found'
      render
    end

    def show_error(exception)
      flash.now[:error] = exception.message
      render
    end

    def sign_in_time
      DateTime.strptime(session[:sign_in_time], "%Y-%m-%d %H:%M:%S")
    end

    def facebook
      @facebook ||= Koala::Facebook::API.new(facebook_access_token)
    end

    def initialize_facebook!
      if @current_user.nil?
        @oauth = Koala::Facebook::OAuth.new(ENV['FACEBOOK_APP_ID'], ENV['FACEBOOK_APP_SECRET'], ENV['FACEBOOK_CALLBACK_URL'])
        @fb_url = @oauth.url_for_oauth_code({ permissions: ENV['FACEBOOK_PERMISSIONS'] })
      end
    end

    def facebook_access_token
      session[:facebook_access_token]
    end

    def visitor_location
      @visitor_location ||= request.location
      store_current_location(@visitor_location.data['latitude'], @visitor_location.data['longitude']) unless @visitor_location.data['city'].blank?
      @visitor_location
    end

    def store_current_location(latitude, longitude)
      session[:current_latitude] = latitude
      session[:current_longitude] = longitude
    end

    def api_inaccessible
      flash.now[:error] = 'Connection Lost, Please Try Again.'
      render
    end

    def sign_in_time
      DateTime.strptime(session[:sign_in_time], "%Y-%m-%d %H:%M:%S")
    end

    def load_current_user_data!
      unless session[:facebook_access_token].blank?
        @current_user ||= User.find session[:current_user]['id']
        @profile_small_picture ||= session[:current_user]['small_picture']
        @profile_big_picture ||= session[:current_user]['big_picture']
      end
    end
end
