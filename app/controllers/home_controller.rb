class HomeController < ApplicationController
  def index
    @recent_posts = Post.recent
    gon.recent_posts = @recent_posts
  end

  def show
    @application_status = @current_user.application_status(params[:id])
    @post = Post.find(params[:id])
    gon.post_location = { latitude: @post.latitude, longitude: @post.longitude }
  end

  def create
  end

  def update
    if params[:submit_application] == '1'
      @current_user.user_post_statuses.create({ remarks: params[:remarks], status: 'applied', post_id: params[:id] })
      flash.now[:success] = 'Application Submitted.'
      post = Post.find params[:id]
      ::Chikka::Send_Api.deliver({message: "Hi! Thank you for applying this #{post.category} - #{post.title}", number: @current_user.contact})
      return redirect_to home_path(id: params[:id], active_tab: params[:active_tab]), turbolinks: true
    end
  end

  def new
    if params[:new_post].present?
      ::Chikka::Send_Api.deliver({message: 'Hi! New Post available.', number: '639066050188'})
      ::Chikka::Send_Api.deliver({message: 'Hi! New Post available.', number: '639267117480'})
      render json: {success: true}, status: :ok
    end
  end

  def apply
  end

  def about_us
  end
end
