class Api::UsersController < ApplicationController
  skip_before_action :load_current_user_data!
  skip_before_action :send_variables_to_js
  
  def create
    if params[:latitude] && params[:longitude]
      store_current_location(params[:latitude], params[:longitude])
    end

    render text: nil
  end
end
