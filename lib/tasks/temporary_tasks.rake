namespace :users_table do
  desc 'generate agency users'
  task generate_agency_users_table: :environment do
    10.times do |i|
      type = (i%2 == 0) ? 'Agency' : 'Ofw'
      user = User.new
      user.full_name = FFaker::Name.name
      user.email = FFaker::Internet.email
      user.account_type = type
      user.save
    end
  end

  desc 'generate post'
  task generate_post_table: :environment do
    20.times do |i|
      title = ['Nurse', 'Makati Palace Unit',
          'Seaman', 'Bahay Mo Aparment',
          'Clerk', 'Human House For Rent',
          'Singer', 'Girls Dormitory',
          'Civil Engineer', 'Boys Dormitory',
          'Teacher', 'Ladies Only',
          'Doctor', 'Boys Only',
          'Accountant', 'House for Sale',
          'IT', 'Studio Type for Rent',
          'Software Engineer', 'Room for Rent']
      type = (i%2 == 0) ? 'job' : 'rent'
      latitude = ['14.5500', '14.5800', '14.5500', '14.5750', '3.1333',
        '25.2048', '1.3147308', '10.2800', '35.6833', '34.6939',
        '14.5500', '14.6572', '14.5351', '22.1497', '14.5833',
        '14.2990183', '15.0000', '14.584245', '10.308757', '14.5583963']
      longitude = ['121.0300', '121.0300', '121.0500', '121.0833', '101.7000',
        '55.2708', '103.8470128', '123.9000', '139.6833', '135.5022',
        '121.0000', '121.0558', '120.9822', '113.5661', '120.9750',
        '120.9589699', '121.0833', '121.057158', '123.893593', '121.0223906']
      post = Post.new
      post.title =  title[i]
      post.description = "#{title[i]} - #{type.humanize} description is here"
      post.latitude = latitude[i]
      post.longitude = longitude[i]
      post.category = type
      post.price = 150
      post.address = 'Address here, City, Country'
      post.user_id = (i + 1)
      post.save
    end
  end
end