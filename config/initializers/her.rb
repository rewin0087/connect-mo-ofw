PLACES_API = Her::API.new
PLACES_API.setup url: ENV['HERE_MAP_PLACES_API'] do |c|
  # Response
  c.use Her::Middleware::DefaultParseJSON
  c.use Faraday::Response::Logger, logger = Rails.logger
  # Adapter
  c.use Faraday::Adapter::NetHttp
end

REVERSE_GEOCODER_API = Her::API.new
REVERSE_GEOCODER_API.setup url: ENV['HERE_MAP_REVERSE_GEOCODER'] do |c|
  # Response
  c.use Her::Middleware::DefaultParseJSON
  c.use Faraday::Response::Logger, logger = Rails.logger
  # Adapter
  c.use Faraday::Adapter::NetHttp
end

SEND_CHIKKA_API = Her::API.new
SEND_CHIKKA_API.setup url: ENV['CHIKKA_SEND_API'] do |c|
  # Response
  c.use Her::Middleware::DefaultParseJSON
  c.use Faraday::Request::UrlEncoded
  c.use Faraday::Response::Logger, logger = Rails.logger
  # Adapter
  c.use Faraday::Adapter::NetHttp
end

RECEIVE_CHIKKA_API = Her::API.new
RECEIVE_CHIKKA_API.setup url: ENV['CHIKKA_SEND_API'] do |c|
  # Response
  c.use Her::Middleware::DefaultParseJSON
  c.use Faraday::Response::Logger, logger = Rails.logger
  # Adapter
  c.use Faraday::Adapter::NetHttp
end